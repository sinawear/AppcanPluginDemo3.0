package com.test;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.zywx.wbpalmstar.engine.universalex.EUExUtil;
import org.zywx.wbpalmstar.widgetone.uexDemo.R;

import view.CalendarGridView;
import view.CalendarGridViewAdapter;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class DateNativeActivity extends Activity {

	private LinearLayout layout_main_content;
	private CalendarGridView gridview_date;
	
	private DateNativeActivity activity = this;
	private CalendarGridViewAdapter adapter;
	private TextView tv_dest;
	private TextView tv_src;
	private LinearLayout layout_src;
	private LinearLayout layout_dest;
	private GridView title_grid_view;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		
		
	
		
		
		int id_xml = EUExUtil.getResLayoutID("date") ;
		setContentView( id_xml );
		
		layout_main_content = (LinearLayout)findViewById( EUExUtil.getResIdID("layout_main_content")  );
		gridview_date = (CalendarGridView)findViewById( EUExUtil.getResIdID("gridview_date"));
		title_grid_view = (GridView)findViewById( EUExUtil.getResIdID("title_grid_view"));
		
		tv_src = (TextView)findViewById( EUExUtil.getResIdID("tv_src"));
		tv_dest = (TextView)findViewById( EUExUtil.getResIdID("tv_dest"));
		layout_src = (LinearLayout)findViewById( EUExUtil.getResIdID("layout_src"));
		layout_dest = (LinearLayout)findViewById( EUExUtil.getResIdID("layout_dest"));
		initListener();
		
		
		
		setTitleDate();
		
		///* 星期日到 星期六的一条格子
		setTitleGirdView();
	//	RelativeLayout.LayoutParams params_cal_title = new RelativeLayout.LayoutParams(
	//			LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
	//	params_cal_title.addRule(RelativeLayout.BELOW, titleLayoutID);
	//	mainLayout.addView(title_grid_view, params_cal_title);
		//*/
				
		
		
		
		Calendar tempSelected1 = Calendar.getInstance(); 
		adapter = new CalendarGridViewAdapter(activity, tempSelected1);
		gridview_date.setAdapter(adapter);
		
		// 
		
	
		//多gridview;
		// 用ExpandListView
		// 
		
		
		
	}
	
	
	private void setTitleDate(){
		Calendar tempSelected1 = Calendar.getInstance(); 
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月");
		TextView tv_date_content = (TextView)findViewById( EUExUtil.getResIdID("tv_date_content"));
		tv_date_content.setText( sdf.format(tempSelected1.getTime()));
		
	}
	
	private void setTitleGirdView() {
		
		///*
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
				LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		title_grid_view.setLayoutParams(params);
		title_grid_view.setNumColumns(7);// 设置每行列数
		title_grid_view.setGravity(Gravity.CENTER_VERTICAL);// 位置居中
		title_grid_view.setBackgroundColor(getResources().getColor(
				R.color.title_text_7));

		WindowManager windowManager = getWindowManager();
		Display display = windowManager.getDefaultDisplay();
		int i = display.getWidth() / 7;
		int j = display.getWidth() - (i * 7);
		int x = j / 2;
		title_grid_view.setPadding(x, 0, 0, 0);// 居中
		
		title_grid_view.setVerticalSpacing(0);// 垂直间隔
		title_grid_view.setHorizontalSpacing(0);// 水平间隔
		//*/
		
//		title_grid_view.setBackgroundColor(getResources().getColor(
//				R.color.title_text_7));
		
		TitleGridAdapter titleAdapter = new TitleGridAdapter(this);
		title_grid_view.setAdapter(titleAdapter);// 设置菜单Adapter
		
	}
	

	
	void initListener(){
		layout_src.setOnClickListener( new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Toast.makeText(activity, "使用出发时间", Toast.LENGTH_SHORT).show();
				// 修改出发时间
				adapter.enable_srcdest_src();
			}
		});
		
		layout_dest.setOnClickListener( new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(activity, "使用返回时间", Toast.LENGTH_SHORT).show();
				
				// 修改返回时间
				adapter.enable_srcdest_dest();
			}
		});
		
	}
	
	
	
	
	public void adapter_update(Date src, Date dest){
		
		SimpleDateFormat dateformat =new SimpleDateFormat("yyyy-MM-dd ");
		  
		tv_src.setText( dateformat.format(src) );
		tv_dest.setText( dateformat.format(dest) );
	}
	
	
	
	

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent in = new Intent(getIntent().getAction());
			setResult(Activity.RESULT_CANCELED, in);
			finish();
			return true;
		}
		return super.onKeyUp(keyCode, event);
	}
	
	
	
	
	//
	public class TitleGridAdapter extends BaseAdapter {

		int[] titles = new int[] { R.string.Sun, R.string.Mon, R.string.Tue,
				R.string.Wed, R.string.Thu, R.string.Fri, R.string.Sat };

		private Activity activity;

		// construct
		public TitleGridAdapter(Activity a) {
			activity = a;
		}

		@Override
		public int getCount() {
			return titles.length;
		}

		@Override
		public Object getItem(int position) {
			return titles[position];
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			LinearLayout iv = new LinearLayout(activity);
			TextView txtDay = new TextView(activity);
			txtDay.setFocusable(false);
			txtDay.setBackgroundColor(Color.TRANSPARENT);
			iv.setOrientation(LinearLayout.HORIZONTAL);

			txtDay.setGravity(Gravity.CENTER);
			LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
					LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);

			int i = (Integer) getItem(position);

			txtDay.setTextColor(Color.WHITE);
			Resources res = getResources();

			if (i == R.string.Sat) {
				// 周六
		//		txtDay.setBackgroundColor(res.getColor(R.color.title_text_6));
			} else if (i == R.string.Sun) {
				// 周日
		//		txtDay.setBackgroundColor(res.getColor(R.color.title_text_7));
			} else {

			}

			txtDay.setText((Integer) getItem(position));

			iv.addView(txtDay, lp);

			return iv;
		}

		
		
	}
	
	
	

}
