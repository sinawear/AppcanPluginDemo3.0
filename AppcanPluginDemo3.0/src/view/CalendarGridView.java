package view;




import org.zywx.wbpalmstar.widgetone.uexDemo.R;

import android.app.Activity;
import android.app.ActivityGroup;
import android.app.ActivityManager;
import android.app.LocalActivityManager;
import android.content.Context;
import android.util.AttributeSet;
import android.view.Display;
import android.view.Gravity;
import android.view.WindowManager;
import android.widget.GridView;
import android.widget.LinearLayout;

public class CalendarGridView extends GridView{
	
	private Context mContext;

	
	public CalendarGridView(Context context) {
		super(context);
		init(context);
	}
	
	public CalendarGridView(Context context, AttributeSet attrs) { 
        super(context, attrs); 
        init(context);
    } 

    
    public CalendarGridView(Context context, AttributeSet attrs, int defStyle) { 
        super(context, attrs, defStyle); 
   
        init(context);
    } 
    
    void init( Context context ){
    	mContext = context;
        
    	if(isInEditMode() == false)
        setGirdView();
    }
    

	private void setGirdView() {
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
				LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		
		setLayoutParams(params);
		setNumColumns(7);// 设置每行列数
		setGravity(Gravity.CENTER_VERTICAL);// 位置居中
		setVerticalSpacing(1);// 垂直间隔
		setHorizontalSpacing(1);// 水平间隔
	//	setBackgroundColor(getResources().getColor(R.color.calendar_background));
	
	//	System.out.println("mContext: "+mContext);
		
		WindowManager windowManager = ((Activity)mContext).getWindowManager();
		Display display = windowManager.getDefaultDisplay();     
		int i = display.getWidth() / 7;
		int j = display.getWidth() - (i * 7);
		int x = j / 2;
		setPadding(x, 0, 0, 0);// 居中
	}
	


    @Override 
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) { 

        int expandSpec = MeasureSpec.makeMeasureSpec( 
                Integer.MAX_VALUE >> 2, MeasureSpec.AT_MOST); 
        super.onMeasure(widthMeasureSpec, expandSpec); 
    } 

	
}
