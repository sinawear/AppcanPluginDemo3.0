package view;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.zywx.wbpalmstar.widgetone.uexDemo.R;

import com.test.DateNativeActivity;

import android.app.Activity;
import android.content.res.Resources;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

public class CalendarGridViewAdapter extends BaseAdapter {

	private Calendar calStartDate = Calendar.getInstance();// 当前显示的日历
	private Calendar calSelected = Calendar.getInstance(); // 选择的日历
	private Calendar calToday = Calendar.getInstance(); // 今日
	

	private Date calSrcDest_Src = calToday.getTime(); // 
	private Date calSrcDest_Dest = calToday.getTime(); 
	
	
	private int iMonthViewCurrentMonth = 0; // 当前视图月

	private DateNativeActivity activity;
	Resources resources;
	ArrayList<java.util.Date> data;
	
	boolean bSrcDest = true;
	boolean bSrcDest_src = false;
	boolean bSrcDest_dest = true;

	int color_calendar_srcdest_src = R.color.calendar_srcdest_src;
	int color_calendar_srcdest_dest = R.color.calendar_srcdest_dest;
	

	// construct
	public CalendarGridViewAdapter(DateNativeActivity a,Calendar cal) {
		calStartDate=cal;
		activity = a;
		resources=activity.getResources();
		data = getDates();
		
		Calendar calDate = Calendar.getInstance();
		calDate.add(Calendar.DATE, 1); // 选择下一天
		calSrcDest_Dest = calDate.getTime();
		
		activity.adapter_update( calSrcDest_Src, calSrcDest_Dest);
	}
	

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		
		
		final LinearLayout iv = new LinearLayout(activity);
		iv.setId(position + 5000);
		iv.setGravity(Gravity.CENTER);
		iv.setOrientation( LinearLayout.VERTICAL );
		iv.setBackgroundColor(resources.getColor(R.color.white));

		final Date item_date = (Date) getItem(position);
		final Calendar item_date_calCalendar = Calendar.getInstance();
		item_date_calCalendar.setTime(item_date); // 设置使用当前时间

		final int iMonth = item_date_calCalendar.get(Calendar.MONTH);
		final int iDay = item_date_calCalendar.get(Calendar.DAY_OF_WEEK);

		// 当日的日期
		TextView txtToDay = new TextView(activity);//  
		txtToDay.setGravity(Gravity.CENTER_HORIZONTAL);
		txtToDay.setTextSize(9);
		if (equalsDate(calToday.getTime(), item_date)) {
			// 当前日期
//			iv.setBackgroundColor(resources.getColor(R.color.event_center));
			txtToDay.setText("TODAY!");
		}

		if( bSrcDest ){
			
			//int offset_day = calSrcDest_Dest.getDate() - calSrcDest_Src.getDate();
			
			// 
			boolean b = InsideDate( calSrcDest_Src, calSrcDest_Dest, item_date );
			if(b){
				iv.setBackgroundColor(resources.getColor(R.color.selection));
			}
			
			if (equalsDate(calSrcDest_Dest, item_date)) { // 
				iv.setBackgroundColor(resources.getColor(R.color.calendar_srcdest_dest));
			}
			
			if (equalsDate(calSrcDest_Src, item_date)) { // 
				iv.setBackgroundColor(resources.getColor(R.color.calendar_srcdest_src));
			}
			
			
			
		}
		else{
			
			// 设置背景颜色
			if (equalsDate(calSelected.getTime(), item_date)) {
				// 选择的
				iv.setBackgroundColor(resources.getColor(R.color.selection));
			} else {
				if (equalsDate(calToday.getTime(), item_date)) {
					// 当前日期
					iv.setBackgroundColor(resources.getColor(R.color.calendar_zhe_day));
				}
			}
			
		}
		
		
		// 日期开始
		TextView txtDay = new TextView(activity);//
		txtDay.setGravity(Gravity.CENTER_HORIZONTAL);

		// 判断是否是当前月
		if (iMonth == iMonthViewCurrentMonth) {
			txtToDay.setTextColor(resources.getColor(R.color.ToDayText));
			txtDay.setTextColor(resources.getColor(R.color.Text));
			
			/*
			// 置当前日期之前为灰色
			if( iDay < calToday.getTime().getDay() ){
				txtDay.setTextColor(resources.getColor(R.color.noMonth));
				txtToDay.setTextColor(resources.getColor(R.color.noMonth));
			}
			*/
			
		} else { // 非当前月置为灰色
			
			txtDay.setTextColor(resources.getColor(R.color.white));
			txtToDay.setTextColor(resources.getColor(R.color.white));
			//txtDay.setVisibility(View.GONE);
			//txtToDay.setVisibility(View.GONE);
		}

		int day = item_date.getDate(); // 日期
		txtDay.setText(String.valueOf(day));
		txtDay.setId(position + 500);
		iv.setTag(item_date);

		// 一个块分成两部分，上面为日期，下面为相关标注
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
				LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		iv.addView(txtDay, lp);

		/*  */
		LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams(
				LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		iv.addView(txtToDay, lp1);
		// 日期结束
		 
		
		///*
		iv.setOnClickListener( new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				//iv.setBackgroundColor(resources.getColor(R.color.selection));
				
				// 当前即为出发日期，
				// 如果目标地址没有选择，选择当前日期的后一天；
				// 
				if(bSrcDest){
					//calSrcDest_Src = data.get(position);
					
					
					// 如果不是当月的日期，就不能点击了
					
			
					
			//		Date item_date = data.get(position);
					
					if( isCurMonthDate(item_date) == false ){ // 如果点击的日期不是当前月份
						return ;
					}
					
					
					if( item_date_calCalendar.compareTo( calToday ) < 0 ){
					//if( item_date.getTime() < calToday.getTime().getTime() ){ // 不能购买当天前的
						return;
					}
					
					
					
					
				//	calSrcDest_Dest = item_date;
					if( bSrcDest_src == true){  // 
						calSrcDest_Src = item_date;
						
						// 如果出发日期大于返程日期，返程日期跟着移动
						if( calSrcDest_Src.compareTo( calSrcDest_Dest ) > 0){
					//	if( calSrcDest_Dest.getTime() < calSrcDest_Src.getTime() ){ // 如果出发日期大于返程日期
							
					//		 color_calendar_srcdest_src = R.color.calendar_srcdest_dest;
					//		 color_calendar_srcdest_dest = R.color.calendar_srcdest_src;
							 
							 // 返程地址
							 Calendar calDate = Calendar.getInstance();
							 calDate.setTime(calSrcDest_Src);
							 calDate.add(Calendar.DATE, 1);
						//	 if(isCurMonthDate(calDate.getTime()))
						//	 {
							 
							 	calSrcDest_Dest = calDate.getTime();
								// calSrcDest_Dest.setTime( calDate.getTime().getTime() ); //原对象值会被改变
								 
						//	 }
							 /*
							 calSrcDest_Dest = calSrcDest_Src;
							 
							Calendar calDate = Calendar.getInstance();
							calDate.add(Calendar.DAY_OF_MONTH, 1); // 
							 calSrcDest_Src = calDate.getTime();
							 */
						}
						else{  //出发日期小于返程日期
							
							
							/*
							 color_calendar_srcdest_src = R.color.calendar_srcdest_dest;
							 color_calendar_srcdest_dest = R.color.calendar_srcdest_src;
							 
							 
							 // 交换时间
							 item_date = calSrcDest_Dest;
							 calSrcDest_Dest = calSrcDest_Src;
							 calSrcDest_Src = item_date;
							 */
							
							
							 
						}
						
						// 如果返程日期小于当天  设置为当天
						
						
					
						
					}
					
					if( bSrcDest_dest == true){ //出发日期
						
						
						if(item_date.getTime() < calSrcDest_Src.getTime()){
					//		Toast.makeText(activity, "出发日期选择有误", Toast.LENGTH_SHORT).show();
							return;
						}
						
						
						calSrcDest_Dest = item_date;
						
						
						/*
						if( calSrcDest_Dest.getTime() < calSrcDest_Src.getTime() ){ // 如果返程日期小于出发
							
							 color_calendar_srcdest_src = R.color.calendar_srcdest_dest;
							 color_calendar_srcdest_dest = R.color.calendar_srcdest_src;
							 
							 // 交换时间
							 item_date = calSrcDest_Dest;
							 calSrcDest_Dest = calSrcDest_Src;
							 calSrcDest_Src = item_date;
							 
						}
						*/
						
					}
					
					
					// 初始化颜色
					color_calendar_srcdest_src = R.color.calendar_srcdest_src;
					color_calendar_srcdest_dest = R.color.calendar_srcdest_dest;
					
					
					
				
					
				
					activity.adapter_update( calSrcDest_Src, calSrcDest_Dest);
					notifyDataSetInvalidated();
				
				
					
					
				}
				
			}
		});
		//*/
		
		

		return iv;
	}
	
	
	
	boolean isCurMonthDate( Date date){
		
		int year = calToday.getTime().getYear();
		int month = calToday.getTime().getMonth();
		
		if( date.getYear() == year && date.getMonth() == month){
			return true;
		}
		
		return false;
	}
	


	private Boolean equalsDate(Date date1, Date date2) {

		if (date1.getYear() == date2.getYear()
				&& date1.getMonth() == date2.getMonth()
				&& date1.getDate() == date2.getDate()) {
			return true;
		} else {
			return false;
		}

	}
	
	private Boolean InsideDate(Date dateStart, Date dateEnd, Date dateCur) {

		boolean b = false;
		
//		int year = date3.getYear();
//		int month = date3.getMonth();
		long timeStart = dateStart.getTime();
		long timeEnd = dateEnd.getTime();
		long time = dateCur.getTime(); 
		
		if( timeStart <= time && time <= timeEnd ){
			b = true;
		}
		
		return b;
		

	}
	
	
	
	public void setSelectedDate(Calendar cal)
	{
		calSelected=cal;
	}
	
	
	// 根据改变的日期更新日历
	// 填充日历控件用
	private void UpdateStartDateForMonth() {
		calStartDate.set(Calendar.DATE, 1); // 设置成当月第一天
		iMonthViewCurrentMonth = calStartDate.get(Calendar.MONTH);// 得到当前日历显示的月

		// 星期一是2 星期天是1 填充剩余天数
		int iDay = 0;
		int iFirstDayOfWeek = Calendar.MONDAY;
		int iStartDay = iFirstDayOfWeek;
		if (iStartDay == Calendar.MONDAY) {
			iDay = calStartDate.get(Calendar.DAY_OF_WEEK) - Calendar.MONDAY;
			if (iDay < 0)
				iDay = 6;
		}
		if (iStartDay == Calendar.SUNDAY) {
			iDay = calStartDate.get(Calendar.DAY_OF_WEEK) - Calendar.SUNDAY;
			if (iDay < 0)
				iDay = 6;
		}
		calStartDate.add(Calendar.DAY_OF_WEEK, -iDay);
		calStartDate.add(Calendar.DAY_OF_MONTH, -1);// 周日第一位

	}
	
	
	private ArrayList<java.util.Date> getDates() {

		UpdateStartDateForMonth();

		ArrayList<java.util.Date> alArrayList = new ArrayList<java.util.Date>();

		for (int i = 1; i <= 42; i++) {
			alArrayList.add(calStartDate.getTime());
			calStartDate.add(Calendar.DAY_OF_MONTH, 1);
		}

		return alArrayList;
	}
	
	
	public void enable_srcdest_src(){  
		bSrcDest_src = true;
		bSrcDest_dest = false;
	}
	
	public void enable_srcdest_dest(){
		bSrcDest_src = false;
		bSrcDest_dest = true;
	}

	// 1）星期  2）选择之前的到底   3）点击对应的时间操作对应的颜色块  
}






